const tabs = document.querySelectorAll(".scrollable-tabs-container a");
const rightArrow = document.querySelectorAll(".scrollable-tabs-container .right-arrow svg");
const leftArrow = document.querySelectorAll(".scrollable-tabs-container .left-arrow svg");

const tabsList = document.querySelectorAll(".scrollable-box");
const leftArrowContainer = document.querySelectorAll(".scrollable-tabs-container .left-arrow");
const rightArrowContainer = document.querySelectorAll(".scrollable-tabs-container .right-arrow");

const removeAllActiveClasses = () => {
  tabs.forEach((tab) => {
    tab.classList.remove("active");
  });
};

tabs.forEach((tab) => {
  tab.addEventListener("click", () => {
    removeAllActiveClasses();
    tab.classList.add("active");
  });
});

const manageIcons = (tabsBox) => {

  if (tabsBox.scrollLeft >= 20) {
    tabsBox.previousElementSibling.classList.add("active");
  } else {
    tabsBox.previousElementSibling.classList.remove("active");
  }

  let maxScrollValue = tabsBox.scrollWidth - tabsBox.clientWidth - 20;
  console.log("scroll width: ", tabsBox.scrollWidth);
  console.log("client width: ", tabsBox.clientWidth);

  if (tabsBox.scrollLeft >= maxScrollValue) {
    tabsBox.nextElementSibling.classList.remove("active");
  } else {
    tabsBox.nextElementSibling.classList.add("active");
  }
};

rightArrow.forEach(arrows => {
  arrows.addEventListener("click", (e) => {
    let tabsBox = e.target.parentElement.previousElementSibling;
    // tabsList.scrollLeft += 200;
    tabsBox.scrollLeft += 200;
    manageIcons(tabsBox);
  })
});


leftArrow.forEach(arrows => {
  arrows.addEventListener("click", (e) => {
    let tabsBox = e.target.parentElement.nextElementSibling;
    tabsBox.scrollLeft -= 200;
    manageIcons(tabsBox);
  })
});


tabsList.forEach(function (current) {
  current.addEventListener("scroll", function (e) {
    let tabCurrent = e.target
    console.log(tabCurrent)
    manageIcons(tabCurrent)
  })
})



let dragging = false;

const drag = (e) => {
  if (!dragging) return;

  // tabsList.forEach(item => {
  e.target.classList.add("dragging");
  // });

  // tabsList.forEach(item => {
  e.target.scrollLeft -= e.movementX;
  // });
};


tabsList.forEach(function (item) {
  // 按下时触发
  item.addEventListener("mousedown", (e) => {
    console.log("按下时触发")
    console.log(e.target)
    dragging = true;
  })
})


tabsList.forEach(item => {
  // 在元素上移动时
  item.addEventListener("mousemove", drag)
  console.log("在元素上移动时")
});

document.addEventListener("mouseup", (e) => {
  // 按钮放开时触发
  console.log("按钮放开时触发")
  dragging = false;

  // tabsList.forEach(item => {
  e.target.classList.remove("dragging");
  // });
});

// ---------------------------------
// const tabs = document.querySelectorAll(".scrollable-tabs-container a");
// const rightArrow = document.querySelector(".scrollable-tabs-container .right-arrow svg");
// const leftArrow = document.querySelector(".scrollable-tabs-container .left-arrow svg");

// const tabsList = document.querySelector(".scrollable-tabs-container ul");
// const leftArrowContainer = document.querySelector(".scrollable-tabs-container .left-arrow");
// const rightArrowContainer = document.querySelector(".scrollable-tabs-container .right-arrow");

// const removeAllActiveClasses = () => {
//   tabs.forEach((tab) => {
//     tab.classList.remove("active");
//   });
// };

// tabs.forEach((tab) => {
//   tab.addEventListener("click", () => {
//     removeAllActiveClasses();
//     tab.classList.add("active");
//   });
// });

// const manageIcons = () => {
//   if (tabsList.scrollLeft >= 20) {
//     leftArrowContainer.classList.add("active");
//   } else {
//     leftArrowContainer.classList.remove("active");
//   }

//   let maxScrollValue = tabsList.scrollWidth - tabsList.clientWidth - 20;
//   console.log("scroll width: ", tabsList.scrollWidth);
//   console.log("client width: ", tabsList.clientWidth);

//   if (tabsList.scrollLeft >= maxScrollValue) {
//     rightArrowContainer.classList.remove("active");
//   } else {
//     rightArrowContainer.classList.add("active");
//   }
// };

// rightArrow.addEventListener("click", () => {
//   tabsList.scrollLeft += 200;
//   manageIcons();
// });

// leftArrow.addEventListener("click", () => {
//   tabsList.scrollLeft -= 200;
//   manageIcons();
// });

// tabsList.addEventListener("scroll", manageIcons);

// let dragging = false;

// const drag = (e) => {
//   if (!dragging) return;
//   tabsList.classList.add("dragging");
//   tabsList.scrollLeft -= e.movementX;
// };

// tabsList.addEventListener("mousedown", () => {
//   dragging = true;
// });

// tabsList.addEventListener("mousemove", drag);

// document.addEventListener("mouseup", () => {
//   dragging = false;
//   tabsList.classList.remove("dragging");
// });